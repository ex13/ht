const express = require('express');
const router = express.Router();
const get = require('lodash.get');
const last = require('lodash.last');
const status = require('http-status');

const {
  getUserList,
  getUser,
  createUser,
  updateUser,
  deleteUser,
} = require('../../services/users');
const { USER_LIST_LIMIT } = require('../../constants');

/**
 * @function
 * @param {any[]} users
 * @param {any} startAfter
 * @returns {Promise<any>}
 */
async function retrieveUsersRecursively(users = [], startAfter = undefined) {
  const { data } = await getUserList({
    limit: USER_LIST_LIMIT,
    starting_after: startAfter,
  });

  users.push(...data);

  if (data.has_more) {
    return retrieveUsersRecursively(users, last(data).id);
  }

  return users;
}

/**
 * Returns a full list of registered users.
 */
router.get('/', async (req, res) => {
  try {
    const users = await retrieveUsersRecursively();
    res.json(users);
  } catch (error) {
    res.send(status.INTERNAL_SERVER_ERROR);
  }
});

/**
 * Returns a single user based on the ID.
 */
router.get('/:id', async (req, res) => {
  try {
    const user = await getUser(req.params.id);
    res.json(user);
  } catch (error) {
    res.send(status.INTERNAL_SERVER_ERROR);
  }
});

/**
 * Creates a new user.
 */
router.post('/', async (req, res) => {
  const {
    email, name, city, street, house, zip,
  } = req.body;

  if (email && name && city && street && house && zip) {
    try {
      const user = await createUser({
        email,
        metadata: {
          name,
          city,
          street,
          house,
          zip,
        },
      });
      res.json(get(user));
    } catch (error) {
      res.send(status.INTERNAL_SERVER_ERROR);
    }
  } else {
    res.send('Bad Request: not all address fields are supplied.', status.BAD_REQUEST);
  }
});

/**
 * Updates a new user.
 */
router.put('/:id', async (req, res) => {
  const {
    email, name, city, street, house, zip,
  } = req.body;

  if (email && name && city && street && house && zip) {
    try {
      const user = await updateUser(
        req.params.id,
        {
          email,
          metadata: {
            name,
            city,
            street,
            house,
            zip,
          },
        }
      );
      res.json(get(user));
    } catch (error) {
      res.send(status.INTERNAL_SERVER_ERROR);
    }
  } else {
    res.send('Bad Request: not all address fields are supplied.', status.BAD_REQUEST);
  }
});

/**
 * Deletes a new user.
 */
router.delete('/:id', async (req, res) => {
  try {
    const confirmation = await deleteUser(req.params.id);
    res.json(confirmation);
  } catch (error) {
    res.send(status.INTERNAL_SERVER_ERROR);
  }
});

module.exports = router;
