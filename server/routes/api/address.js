const express = require('express');
const status = require('http-status');

const { checkGeocode } = require('../../services/geocode');

const router = express.Router();

/**
 * Validates the user address through Google Maps API.
 */
router.post('/', (req, res) => {
  const {
    city, street, house, zip,
  } = req.body;

  if (city && street && house && zip) {
    checkGeocode({
      data: {
        city, street, house, zip,
      },
      onSuccess: () => {
        res.send(status.OK);
      },
      onFailure: () => {
        res.send('Address Not Found.', status.NOT_FOUND);
      },
    });
  } else {
    res.send('Bad Request: not all address fields are supplied.', status.BAD_REQUEST);
  }
});

module.exports = router;
