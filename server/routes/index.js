const express = require('express');
const router = express.Router();

const userApiRouter = require('./api/user');
const addressApiRouter = require('./api/address');

// Index route
router.get('/', (req, res) => {
  res.render('index', { title: 'Server App' });
});

// User CRUD API
router.use('/api/user', userApiRouter);

// Google Maps address validation API
router.use('/api/address', addressApiRouter);

module.exports = router;
