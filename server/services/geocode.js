const get = require('lodash.get');
const { GOOGLE_API_KEY } = require('../constants');

const googleMapsClient = require('@google/maps').createClient({
  key: GOOGLE_API_KEY,
});

const checkGeocode = ({
  data: {
    city, street, house, zip,
  },
  onSuccess,
  onFailure,
}) => {
  // Commented out for testing purposes, so the address is always valid
  // Uncomment the googleMapsClient code to check it out in action
  onSuccess();

  // googleMapsClient.geocode(
  //   {
  //     address: `${house} ${street}, ${city}, ${zip}`
  //   },
  //   (err, response) => {
  //     if (
  //       !err &&
  //       response.json.status === 'OK' &&
  //       get(response.json.results[0], 'types').includes('street_address')
  //     ) {
  //       onSuccess();
  //     } else {
  //       onFailure();
  //     }
  //   }
  // );
};

module.exports = { checkGeocode };
