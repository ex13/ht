const util = require('util');
const { STRIPE_SECRET_KEY } = require('../constants');

// Initialize Stripe API
const stripe = require('stripe')(STRIPE_SECRET_KEY);

// Need to be bound to stripe.customers due to this issue:
// https://github.com/stripe/stripe-node/issues/418
const getUserList = util.promisify(stripe.customers.list).bind(stripe.customers);
const getUser = util.promisify(stripe.customers.retrieve).bind(stripe.customers);
const createUser = util.promisify(stripe.customers.create).bind(stripe.customers);
const updateUser = util.promisify(stripe.customers.update).bind(stripe.customers);
const deleteUser = util.promisify(stripe.customers.del).bind(stripe.customers);

module.exports = {
  getUserList,
  getUser,
  createUser,
  updateUser,
  deleteUser,
};
