# CustomR - customer CRUD management app

![alt logo](src/assets/logo.png "Logo")

## Getting started

1. Clone this repository
2. Enter the cloned folder
3. Install client dependencies: ```yarn install```
4. Install Node.js server dependencies: ```yarn install:server```
5. Launch the client enviroment: ```yarn start```
6. In a separate console window launch the server enviroment (from the same folder): ```yarn server```
7. In your browser navigate to: ```http://localhost:3000```
8. Have fun exploring the app :)

## Useful commands

- ```yarn install``` - install all client dependencies
- ```yarn install:clean``` - reinstall all of the client's dependencies and clean the cache
- ```yarn install:server``` - install all server dependencies
- ```yarn start``` - start a local development server from a client
- ```yarn server``` - start a local development server from a Node.js server
- ```yarn build``` - create a production optimized build
- ```yarn lint``` - launch a code style check using ESLint
- ```yarn test:unit``` - launch automated tests
- ```yarn test``` - launch a linter & all of the automated tests at once
- ```yarn:precommit``` - used to test code in a pre-commit hook

## Some facts related to this app's implementation

- Build with `ReactJS`
- Uses `Redux` for state management
- Uses `Redux-saga` to make application's side effects easier to manage and test
- It's a single page application. Uses `React Router 4`
- Client is built on top of the Facebook's `create-react-app`
- Back-end server (or more like a middle-end) is built on top of the `Express.js` framework
- `Stripe API` is used to store and retrieve user data
- `ESlint` is configured for code style linting
- `Jest` is used for tests, standard configuration
- `Smooth UI` framework is used on the front-end side for grid, buttons, modal, forms, etc.
- Styles are written using the `styled-components` library
- Repo has a Git `pre-commit hook` setup for additional quality assuranse before committing changes
- Additionally there is an .editorconfig file included, which could enforce some basic rules
- Google API address validation is implemented, but is commented out right now because of the following reasons:
  - My secret API key for Google Geocoding has a limit on the number of requests, and is being blocked by Google
  - When requests are blocked, app considers it as a failing validation (because based on the requirements addresses should be validated before one can create a new user entry)
  - And when validation of the address is failing, there's no possibility to create, edit users in the app
  - See more notes on this below

## Google Maps API validation

Right now the API, which is handling the Google Maps validation on the app's back-end server, always returns a successful `200` status. Validation is turned off basically.

To test it:
1. Go to this file: `server/routes/api/address.js`
2. Comment the line #19
3. Uncomment the code on the lines #21 through #36
4. In this file: `server/constants.js` paste a working GOOGLE_API_KEY instead of the existing one
5. Thank you for your marvelous patience! Also, sorry that everything is in one commit :)

Cheers!
