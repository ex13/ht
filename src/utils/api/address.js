import axios from 'axios';
import get from 'lodash.get';

import { API_ADDRESS_FULL } from './endpoints';

/**
 * Validates an address usign Google Maps API.
 *
 * @param  {Object} data
 *
 * @throws Will throw an error if the request goes wrong.
 */
export const validateAddress = ({ data = {} }) => axios.post(API_ADDRESS_FULL, data)
  .then(response => get(response, 'data'))
  .catch(() => {
    throw new Error('Something went wrong in the address validation API.');
  });
