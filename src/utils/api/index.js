import {
  getUserList,
  registerUser,
  getUser,
  updateUser,
  deleteUser,
} from './user';
import { validateAddress } from './address';

// A collection of all API handlers
export {
  getUserList,
  registerUser,
  getUser,
  updateUser,
  deleteUser,
  validateAddress,
};
