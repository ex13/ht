import axios from 'axios';
import get from 'lodash.get';

import { API_USER_FULL } from './endpoints';

/**
 * Requests a user list from the API.
 *
 * @return {Array} userList
 * @throws Will throw an error if the request goes wrong.
 */
export const getUserList = () => axios.get(API_USER_FULL)
  .then(response => get(response, 'data'))
  .catch(() => {
    throw new Error('Something went wrong in the user list API');
  });

/**
 * Requests a single user from the API.
 *
 * @return {Object} user
 * @throws Will throw an error if the request goes wrong.
 */
export const getUser = ({ id }) => axios.get(`${API_USER_FULL}/${id}`)
  .then(response => get(response, 'data'))
  .catch(() => {
    throw new Error('Something went wrong in the user retrieve API');
  });

/**
 * Creates a new user.
 *
 * @param  {Object} data
 *
 * @throws Will throw an error if the request goes wrong.
 */
export const registerUser = ({ data = {} }) => axios.post(API_USER_FULL, data)
  .then(response => get(response, 'data'))
  .catch(() => {
    throw new Error('Something went wrong in the user API while creating a new user');
  });

/**
 * Updates an existing user.
 *
 * @param  {Object} data
 *
 * @throws Will throw an error if the request goes wrong.
 */
export const updateUser = ({ id, data = {} }) => axios.put(`${API_USER_FULL}/${id}`, data)
  .then(response => get(response, 'data'))
  .catch(() => {
    throw new Error('Something went wrong in the user API while updating a new user');
  });

/**
 * Deletes an existing user.
 *
 * @param  {Object} data
 *
 * @throws Will throw an error if the request goes wrong.
 */
export const deleteUser = ({ id }) => axios.delete(`${API_USER_FULL}/${id}`)
  .then(response => get(response, 'data'))
  .catch(() => {
    throw new Error('Something went wrong in the user API while deleting a new user');
  });
