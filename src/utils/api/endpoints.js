// Base url of the API endpoint
export const API_DOMAIN = 'http://localhost:3001';

// User endpoint
export const API_USER = '/api/user';
export const API_USER_FULL = `${API_DOMAIN}${API_USER}`;

// Address validation endpoint
export const API_ADDRESS = '/api/address';
export const API_ADDRESS_FULL = `${API_DOMAIN}${API_ADDRESS}`;
