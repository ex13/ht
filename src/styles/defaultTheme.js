import { theme } from '@smooth-ui/core-sc';

export default {
  ...theme,
  // Background colors
  bgColorPrimary: '#fff',
  bgColorSecondary: '#f5f5f5',
  // List colors
  listTextColor: '#333333',
  listBorderColor: '#e6e6e6',
};
