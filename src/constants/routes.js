// Application route URI list
export const ROUTE_HOME = '/';
export const ROUTE_USER_REGISTRATION = '/registration';
export const ROUTE_USER_EDIT = '/edit';
export const ROUTE_USER_DETAILS = '/user';
