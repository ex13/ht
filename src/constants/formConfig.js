import * as Yup from 'yup';
import get from 'lodash.get';

// User registration form configuration
export const REGISTRATION_FORM_CONFIG = {
  enableReinitialize: true,
  mapPropsToValues: props => ({
    name: get(props, 'user.metadata.name', ''),
    email: get(props, 'user.email', ''),
    city: get(props, 'user.metadata.city', ''),
    street: get(props, 'user.metadata.street', ''),
    house: get(props, 'user.metadata.house', ''),
    zip: get(props, 'user.metadata.zip', ''),
  }),
  validationSchema: Yup.object().shape({
    name: Yup.string().required(),
    email: Yup.string().email().required(),
    city: Yup.string().required(),
    street: Yup.string().required(),
    house: Yup.number().required(),
    zip: Yup.string().required(),
  }),
  handleSubmit: (values, {
    setSubmitting, props: {
      registerUser,
      updateUser,
      match: { params: { id } },
    },
  }) => {
    if (id) {
      updateUser(id, values);
    } else {
      registerUser(values);
    }
    setSubmitting(false);
  },
  displayName: 'REGISTRATION_FORM',
};
