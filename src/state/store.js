import { createStore, applyMiddleware } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';

import rootReducer from './reducers';
import {
  watchUserList,
  watchUserRegistration,
  watchUserRetrieve,
  watchUserUpdate,
  watchUserDelete,
  watchAddressValidate,
} from './sagas';

export default function configureStore({ initialState = {}, history }) {
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [
    routerMiddleware(history),
    sagaMiddleware,
  ];

  const store = createStore(
    rootReducer,
    initialState,
    composeWithDevTools(applyMiddleware(...middlewares))
  );

  // Run sagas
  sagaMiddleware.run(watchUserList);
  sagaMiddleware.run(watchUserRegistration);
  sagaMiddleware.run(watchUserRetrieve);
  sagaMiddleware.run(watchUserUpdate);
  sagaMiddleware.run(watchUserDelete);
  sagaMiddleware.run(watchAddressValidate);

  if (module.hot) {
    module.hot.accept('./reducers', () => {
      const nextRootReducer = require('./reducers'); // eslint-disable-line global-require
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
}
