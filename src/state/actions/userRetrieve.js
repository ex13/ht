import {
  USER_REQUESTED,
  USER_RECEIVED,
  USER_ERROR,
  USER_UNLOAD,
} from './userRetrieveTypes';

export const userRetrieveRequest = id => ({
  type: USER_REQUESTED,
  id,
});

export const userRetrieveSuccess = user => ({
  type: USER_RECEIVED,
  user,
});

export const userRetrieveError = error => ({
  type: USER_ERROR,
  error,
});

export const userRetrieveUnload = () => ({
  type: USER_UNLOAD,
});
