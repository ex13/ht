import {
  USER_REGISTRATION_REQUESTED,
  USER_REGISTRATION_SUCCESS,
  USER_REGISTRATION_ERROR,
  USER_REGISTRATION_UNLOAD,
} from './userRegistrationTypes';

export const userRegistrationRequest = data => ({
  type: USER_REGISTRATION_REQUESTED,
  data,
});

export const userRegistrationSuccess = () => ({
  type: USER_REGISTRATION_SUCCESS,
});

export const userRegistrationError = error => ({
  type: USER_REGISTRATION_ERROR,
  error,
});

export const userRegistrationUnload = () => ({
  type: USER_REGISTRATION_UNLOAD,
});
