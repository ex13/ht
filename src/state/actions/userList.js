import {
  USER_LIST_REQUESTED,
  USER_LIST_RECEIVED,
  USER_LIST_ERROR,
} from './userListTypes';

export const userListRequest = () => ({
  type: USER_LIST_REQUESTED,
});

export const userListReceive = userList => ({
  type: USER_LIST_RECEIVED,
  userList,
});

export const userListError = error => ({
  type: USER_LIST_ERROR,
  error,
});
