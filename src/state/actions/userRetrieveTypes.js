export const USER_REQUESTED = 'USER_REQUESTED';
export const USER_RECEIVED = 'USER_RECEIVED';
export const USER_ERROR = 'USER_ERROR';
export const USER_UNLOAD = 'USER_UNLOAD';
