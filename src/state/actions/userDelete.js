import {
  USER_DELETE_REQUESTED,
  USER_DELETE_SUCCESS,
  USER_DELETE_ERROR,
  USER_DELETE_UNLOAD,
} from './userDeleteTypes';

export const userDeleteRequest = id => ({
  type: USER_DELETE_REQUESTED,
  id,
});

export const userDeleteSuccess = () => ({
  type: USER_DELETE_SUCCESS,
});

export const userDeleteError = error => ({
  type: USER_DELETE_ERROR,
  error,
});

export const userDeleteUnload = () => ({
  type: USER_DELETE_UNLOAD,
});
