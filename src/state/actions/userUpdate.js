import {
  USER_UPDATE_REQUESTED,
  USER_UPDATE_SUCCESS,
  USER_UPDATE_ERROR,
  USER_UPDATE_UNLOAD,
} from './userUpdateTypes';

export const userUpdateRequest = (id, data) => ({
  type: USER_UPDATE_REQUESTED,
  id,
  data,
});

export const userUpdateSuccess = () => ({
  type: USER_UPDATE_SUCCESS,
});

export const userUpdateError = error => ({
  type: USER_UPDATE_ERROR,
  error,
});

export const userUpdateUnload = () => ({
  type: USER_UPDATE_UNLOAD,
});
