import {
  ADDRESS_VALIDATE_REQUESTED,
  ADDRESS_VALIDATE_SUCCESS,
  ADDRESS_VALIDATE_ERROR,
  ADDRESS_VALIDATE_UNLOAD,
} from './addressValidateTypes';

export const addressValidateRequest = data => ({
  type: ADDRESS_VALIDATE_REQUESTED,
  data,
});

export const addressValidateSuccess = () => ({
  type: ADDRESS_VALIDATE_SUCCESS,
});

export const addressValidateError = error => ({
  type: ADDRESS_VALIDATE_ERROR,
  error,
});

export const addressValidateUnload = () => ({
  type: ADDRESS_VALIDATE_UNLOAD,
});
