import {
  userRetrieveRequest,
  userRetrieveSuccess,
  userRetrieveError,
  userRetrieveUnload,
} from '../userRetrieve';
import {
  USER_REQUESTED,
  USER_RECEIVED,
  USER_ERROR,
  USER_UNLOAD,
} from '../userRetrieveTypes';

describe('userRetrieve actions', () => {
  it('should fire with expected props.', () => {
    const requestPayload = {
      type: USER_REQUESTED,
      id: '123',
    };

    const successPayload = {
      type: USER_RECEIVED,
    };

    const errorPayload = {
      type: USER_ERROR,
      error: 'Error',
    };

    const unloadPayload = {
      type: USER_UNLOAD,
    };

    expect(userRetrieveRequest(requestPayload.id)).toEqual(requestPayload);
    expect(userRetrieveSuccess()).toEqual(successPayload);
    expect(userRetrieveError(errorPayload.error)).toEqual(errorPayload);
    expect(userRetrieveUnload()).toEqual(unloadPayload);
  });
});
