import {
  userUpdateRequest,
  userUpdateSuccess,
  userUpdateError,
  userUpdateUnload,
} from '../userUpdate';
import {
  USER_UPDATE_REQUESTED,
  USER_UPDATE_SUCCESS,
  USER_UPDATE_ERROR,
  USER_UPDATE_UNLOAD,
} from '../userUpdateTypes';

describe('userUpdate actions', () => {
  it('should fire with expected props.', () => {
    const requestPayload = {
      type: USER_UPDATE_REQUESTED,
      id: '123',
      data: {},
    };

    const successPayload = {
      type: USER_UPDATE_SUCCESS,
    };

    const errorPayload = {
      type: USER_UPDATE_ERROR,
      error: 'Error',
    };

    const unloadPayload = {
      type: USER_UPDATE_UNLOAD,
    };

    expect(userUpdateRequest(requestPayload.id, requestPayload.data)).toEqual(requestPayload);
    expect(userUpdateSuccess()).toEqual(successPayload);
    expect(userUpdateError(errorPayload.error)).toEqual(errorPayload);
    expect(userUpdateUnload()).toEqual(unloadPayload);
  });
});
