import {
  userDeleteRequest,
  userDeleteSuccess,
  userDeleteError,
  userDeleteUnload,
} from '../userDelete';
import {
  USER_DELETE_REQUESTED,
  USER_DELETE_SUCCESS,
  USER_DELETE_ERROR,
  USER_DELETE_UNLOAD,
} from '../userDeleteTypes';

describe('userDelete actions', () => {
  it('should fire with expected props.', () => {
    const requestPayload = {
      type: USER_DELETE_REQUESTED,
      id: '123',
    };

    const successPayload = {
      type: USER_DELETE_SUCCESS,
    };

    const errorPayload = {
      type: USER_DELETE_ERROR,
      error: 'Error',
    };

    const unloadPayload = {
      type: USER_DELETE_UNLOAD,
    };

    expect(userDeleteRequest(requestPayload.id)).toEqual(requestPayload);
    expect(userDeleteSuccess()).toEqual(successPayload);
    expect(userDeleteError(errorPayload.error)).toEqual(errorPayload);
    expect(userDeleteUnload()).toEqual(unloadPayload);
  });
});
