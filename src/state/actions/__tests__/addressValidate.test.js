import {
  addressValidateRequest,
  addressValidateSuccess,
  addressValidateError,
  addressValidateUnload,
} from '../addressValidate';
import {
  ADDRESS_VALIDATE_REQUESTED,
  ADDRESS_VALIDATE_SUCCESS,
  ADDRESS_VALIDATE_ERROR,
  ADDRESS_VALIDATE_UNLOAD,
} from '../addressValidateTypes';

describe('addressValidate actions', () => {
  it('should fire with expected props.', () => {
    const requestPayload = {
      type: ADDRESS_VALIDATE_REQUESTED,
      data: {},
    };

    const successPayload = {
      type: ADDRESS_VALIDATE_SUCCESS,
    };

    const errorPayload = {
      type: ADDRESS_VALIDATE_ERROR,
      error: 'Error',
    };

    const unloadPayload = {
      type: ADDRESS_VALIDATE_UNLOAD,
    };

    expect(addressValidateRequest(requestPayload.data)).toEqual(requestPayload);
    expect(addressValidateSuccess()).toEqual(successPayload);
    expect(addressValidateError(errorPayload.error)).toEqual(errorPayload);
    expect(addressValidateUnload()).toEqual(unloadPayload);
  });
});
