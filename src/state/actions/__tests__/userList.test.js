import {
  userListRequest,
  userListReceive,
  userListError,
} from '../userList';
import {
  USER_LIST_REQUESTED,
  USER_LIST_RECEIVED,
  USER_LIST_ERROR,
} from '../userListTypes';

describe('userList actions', () => {
  it('should fire with expected props.', () => {
    const requestPayload = {
      type: USER_LIST_REQUESTED,
    };

    const successPayload = {
      type: USER_LIST_RECEIVED,
      userList: [
        { id: '1' },
        { id: '2' },
        { id: '3' },
      ],
    };

    const errorPayload = {
      type: USER_LIST_ERROR,
      error: 'Error',
    };

    expect(userListRequest()).toEqual(requestPayload);
    expect(userListReceive(successPayload.userList)).toEqual(successPayload);
    expect(userListError(errorPayload.error)).toEqual(errorPayload);
  });
});
