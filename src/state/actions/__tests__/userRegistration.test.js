import {
  userRegistrationRequest,
  userRegistrationSuccess,
  userRegistrationError,
  userRegistrationUnload,
} from '../userRegistration';
import {
  USER_REGISTRATION_REQUESTED,
  USER_REGISTRATION_SUCCESS,
  USER_REGISTRATION_ERROR,
  USER_REGISTRATION_UNLOAD,
} from '../userRegistrationTypes';

describe('userDelete actions', () => {
  it('should fire with expected props.', () => {
    const requestPayload = {
      type: USER_REGISTRATION_REQUESTED,
      data: {},
    };

    const successPayload = {
      type: USER_REGISTRATION_SUCCESS,
    };

    const errorPayload = {
      type: USER_REGISTRATION_ERROR,
      error: 'Error',
    };

    const unloadPayload = {
      type: USER_REGISTRATION_UNLOAD,
    };

    expect(userRegistrationRequest(requestPayload.data)).toEqual(requestPayload);
    expect(userRegistrationSuccess()).toEqual(successPayload);
    expect(userRegistrationError(errorPayload.error)).toEqual(errorPayload);
    expect(userRegistrationUnload()).toEqual(unloadPayload);
  });
});
