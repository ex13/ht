import { call, put, takeLatest } from 'redux-saga/effects';

import { registerUser } from '../../utils/api';
import { USER_REGISTRATION_REQUESTED } from '../actions/userRegistrationTypes';
import {
  userRegistrationSuccess,
  userRegistrationError,
} from '../actions/userRegistration';

export function* userRegistration(data) {
  try {
    const userRegistrationCall = yield call(registerUser, data);
    yield put(userRegistrationSuccess(userRegistrationCall));
  } catch (error) {
    yield put(userRegistrationError(error));
  }
}

export function* watchUserRegistration() {
  yield takeLatest(USER_REGISTRATION_REQUESTED, userRegistration);
}
