import { call } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';

import {
  userRegistrationRequest,
  userRegistrationSuccess,
  userRegistrationError,
} from '../../actions/userRegistration';
import { userRegistration } from '../userRegistration';
import userRegistrationReducer from '../../reducers/userRegistration';
import { registerUser } from '../../../utils/api';

describe('userRegistration sagas', () => {
  const data = {
    name: 'FakeName',
    email: 'fake@email.com',
    city: 'Vilnius',
    street: 'Kazimiro',
    house: 22,
    zip: 'LT12345',
  };
  let initialState;

  beforeEach(() => {
    initialState = {
      userCreated: null,
      isLoading: false,
      error: null,
    };
  });

  it('should successfully register a user.', () => {
    const expectedState = { ...initialState, userCreated: true };

    return expectSaga(userRegistration, data)
      .provide([
        [call(registerUser)],
        [matchers.call.fn(registerUser), data],
      ])
      .withReducer(userRegistrationReducer, initialState)
      .put(userRegistrationSuccess())
      .dispatch(userRegistrationRequest)
      .hasFinalState(expectedState)
      .run();
  });

  it('should throw an error if the wrong data was passed in.', () => {
    const error = 'Error';
    const expectedState = {
      ...initialState,
      error,
    };

    return expectSaga(userRegistration, {})
      .provide([
        [call(registerUser)],
        [matchers.call.fn(registerUser), throwError(error)],
      ])
      .withReducer(userRegistrationReducer, initialState)
      .put(userRegistrationError(error))
      .dispatch(userRegistrationRequest)
      .hasFinalState(expectedState)
      .run();
  });
});
