import { call } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';

import {
  userUpdateRequest,
  userUpdateSuccess,
  userUpdateError,
} from '../../actions/userUpdate';
import { userUpdate } from '../userUpdate';
import userUpdateReducer from '../../reducers/userUpdate';
import { updateUser } from '../../../utils/api';

describe('userUpdate sagas', () => {
  const user = {
    name: 'FakeName',
    email: 'fake@email.com',
    city: 'Vilnius',
    street: 'Kazimiro',
    house: 22,
    zip: 'LT12345',
  };
  let initialState;

  beforeEach(() => {
    initialState = {
      userUpdated: null,
      isLoading: false,
      error: null,
    };
  });

  it('should successfully update an existing user.', () => {
    const expectedState = { ...initialState, userUpdated: true };

    return expectSaga(userUpdate, user)
      .provide([
        [call(updateUser)],
        [matchers.call.fn(updateUser), user],
      ])
      .withReducer(userUpdateReducer, initialState)
      .put(userUpdateSuccess(user))
      .dispatch(userUpdateRequest)
      .hasFinalState(expectedState)
      .run();
  });

  it('should throw an error if the wrong data was passed in.', () => {
    const error = 'Error';
    const expectedState = {
      ...initialState,
      error,
    };

    return expectSaga(userUpdate, {})
      .provide([
        [call(updateUser)],
        [matchers.call.fn(updateUser), throwError(error)],
      ])
      .withReducer(userUpdateReducer, initialState)
      .put(userUpdateError(error))
      .dispatch(userUpdateRequest)
      .hasFinalState(expectedState)
      .run();
  });
});
