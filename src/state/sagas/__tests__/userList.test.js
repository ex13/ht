import { call } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';

import {
  userListRequest,
  userListReceive,
  userListError,
} from '../../actions/userList';
import { userList } from '../userList';
import userListReducer from '../../reducers/userList';
import { getUserList } from '../../../utils/api';

describe('userList sagas', () => {
  let initialState;
  const userListMock = [
    { id: '1' },
    { id: '2' },
    { id: '3' },
  ];

  beforeEach(() => {
    initialState = {
      userList: [],
      isLoading: false,
      error: null,
    };
  });

  it('should receive a user list successfully.', () => {
    const expectedState = { ...initialState, userList: userListMock };

    return expectSaga(userList, userListMock)
      .provide([
        [call(getUserList)],
        [matchers.call.fn(getUserList), userListMock],
      ])
      .withReducer(userListReducer, initialState)
      .put(userListReceive(userListMock))
      .dispatch(userListRequest)
      .hasFinalState(expectedState)
      .run();
  });

  it('should throw an error if there is a problem on a server.', () => {
    const error = 'Error';
    const expectedState = {
      ...initialState,
      error,
    };

    return expectSaga(userList)
      .provide([
        [call(getUserList)],
        [matchers.call.fn(getUserList), throwError(error)],
      ])
      .withReducer(userListReducer, initialState)
      .put(userListError(error))
      .dispatch(userListRequest)
      .hasFinalState(expectedState)
      .run();
  });
});
