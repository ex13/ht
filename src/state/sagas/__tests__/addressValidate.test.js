import { call } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';

import {
  addressValidateRequest,
  addressValidateSuccess,
  addressValidateError,
} from '../../actions/addressValidate';
import { addressValidate } from '../addressValidate';
import addressValidateReducer from '../../reducers/addressValidate';
import { validateAddress } from '../../../utils/api';

describe('addressValidate sagas', () => {
  const data = {
    city: 'Vilnius',
    street: 'Kazimiro',
    house: 22,
    zip: 'LT12345',
  };
  let initialState;

  beforeEach(() => {
    initialState = {
      validated: true,
      isLoading: false,
      error: null,
    };
  });

  it('should validate adress data successfully.', () => {
    const expectedState = { ...initialState };

    return expectSaga(addressValidate, data)
      .provide([
        [call(validateAddress)],
        [matchers.call.fn(validateAddress), data],
      ])
      .withReducer(addressValidateReducer, initialState)
      .put(addressValidateSuccess())
      .dispatch(addressValidateRequest)
      .hasFinalState(expectedState)
      .run();
  });

  it('should throw an error if the wrong data was passed in.', () => {
    const error = 'Error';
    const expectedState = {
      ...initialState,
      error,
      validated: false,
    };

    return expectSaga(addressValidate, {})
      .provide([
        [call(validateAddress)],
        [matchers.call.fn(validateAddress), throwError(error)],
      ])
      .withReducer(addressValidateReducer, initialState)
      .put(addressValidateError(error))
      .dispatch(addressValidateRequest)
      .hasFinalState(expectedState)
      .run();
  });
});
