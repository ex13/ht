import { call } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';

import {
  userRetrieveRequest,
  userRetrieveSuccess,
  userRetrieveError,
} from '../../actions/userRetrieve';
import { userRetrieve } from '../userRetrieve';
import userRetrieveReducer from '../../reducers/userRetrieve';
import { getUser } from '../../../utils/api';

describe('userRetrieve sagas', () => {
  const user = {
    name: 'FakeName',
    email: 'fake@email.com',
    city: 'Vilnius',
    street: 'Kazimiro',
    house: 22,
    zip: 'LT12345',
  };
  let initialState;

  beforeEach(() => {
    initialState = {
      user: {},
      isLoading: false,
      error: null,
    };
  });

  it('should successfully retrieve a user.', () => {
    const expectedState = { ...initialState, user };

    return expectSaga(userRetrieve, user)
      .provide([
        [call(getUser)],
        [matchers.call.fn(getUser), user],
      ])
      .withReducer(userRetrieveReducer, initialState)
      .put(userRetrieveSuccess(user))
      .dispatch(userRetrieveRequest)
      .hasFinalState(expectedState)
      .run();
  });

  it('should throw an error if the wrong data was passed in.', () => {
    const error = 'Error';
    const expectedState = {
      ...initialState,
      error,
    };

    return expectSaga(userRetrieve, {})
      .provide([
        [call(getUser)],
        [matchers.call.fn(getUser), throwError(error)],
      ])
      .withReducer(userRetrieveReducer, initialState)
      .put(userRetrieveError(error))
      .dispatch(userRetrieveRequest)
      .hasFinalState(expectedState)
      .run();
  });
});
