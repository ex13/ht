import { call } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';

import {
  userDeleteRequest,
  userDeleteSuccess,
  userDeleteError,
} from '../../actions/userDelete';
import { userDelete } from '../userDelete';
import userDeleteReducer from '../../reducers/userDelete';
import { deleteUser } from '../../../utils/api';

describe('userDelete sagas', () => {
  const id = '12345';
  let initialState;

  beforeEach(() => {
    initialState = {
      userDeleted: null,
      isLoading: false,
      error: null,
    };
  });

  it('should delete user successfully.', () => {
    const expectedState = { ...initialState, userDeleted: true };

    return expectSaga(userDelete, id)
      .provide([
        [call(deleteUser)],
        [matchers.call.fn(deleteUser), id],
      ])
      .withReducer(userDeleteReducer, initialState)
      .put(userDeleteSuccess())
      .dispatch(userDeleteRequest)
      .hasFinalState(expectedState)
      .run();
  });

  it('should throw an error if the wrong data was passed in.', () => {
    const error = 'Error';
    const expectedState = {
      ...initialState,
      error,
    };

    return expectSaga(userDelete, {})
      .provide([
        [call(deleteUser)],
        [matchers.call.fn(deleteUser), throwError(error)],
      ])
      .withReducer(userDeleteReducer, initialState)
      .put(userDeleteError(error))
      .dispatch(userDeleteRequest)
      .hasFinalState(expectedState)
      .run();
  });
});
