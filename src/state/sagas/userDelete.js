import { call, put, takeLatest } from 'redux-saga/effects';

import { deleteUser } from '../../utils/api';
import { USER_DELETE_REQUESTED } from '../actions/userDeleteTypes';
import {
  userDeleteSuccess,
  userDeleteError,
} from '../actions/userDelete';

export function* userDelete(payload) {
  try {
    const userDeleteCall = yield call(deleteUser, payload);
    yield put(userDeleteSuccess(userDeleteCall));
  } catch (error) {
    yield put(userDeleteError(error));
  }
}

export function* watchUserDelete() {
  yield takeLatest(USER_DELETE_REQUESTED, userDelete);
}
