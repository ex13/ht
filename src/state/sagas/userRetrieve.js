import { call, put, takeLatest } from 'redux-saga/effects';

import { getUser } from '../../utils/api';
import { USER_REQUESTED } from '../actions/userRetrieveTypes';
import {
  userRetrieveSuccess,
  userRetrieveError,
} from '../actions/userRetrieve';

export function* userRetrieve(id) {
  try {
    const userRetrieveCall = yield call(getUser, id);
    yield put(userRetrieveSuccess(userRetrieveCall));
  } catch (error) {
    yield put(userRetrieveError(error));
  }
}

export function* watchUserRetrieve() {
  yield takeLatest(USER_REQUESTED, userRetrieve);
}
