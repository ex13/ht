import { call, put, takeLatest } from 'redux-saga/effects';

import { getUserList } from '../../utils/api';
import { USER_LIST_REQUESTED } from '../actions/userListTypes';
import {
  userListReceive,
  userListError,
} from '../actions/userList';

export function* userList(payload) {
  try {
    const userListCall = yield call(getUserList, payload);
    yield put(userListReceive(userListCall));
  } catch (error) {
    yield put(userListError(error));
  }
}

export function* watchUserList() {
  yield takeLatest(USER_LIST_REQUESTED, userList);
}
