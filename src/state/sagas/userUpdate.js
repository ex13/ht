import { call, put, takeLatest } from 'redux-saga/effects';

import { updateUser } from '../../utils/api';
import { USER_UPDATE_REQUESTED } from '../actions/userUpdateTypes';
import {
  userUpdateSuccess,
  userUpdateError,
} from '../actions/userUpdate';

export function* userUpdate(payload) {
  try {
    const userUpdateCall = yield call(updateUser, payload);
    yield put(userUpdateSuccess(userUpdateCall));
  } catch (error) {
    yield put(userUpdateError(error));
  }
}

export function* watchUserUpdate() {
  yield takeLatest(USER_UPDATE_REQUESTED, userUpdate);
}
