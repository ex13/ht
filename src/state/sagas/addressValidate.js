import { call, put, takeLatest } from 'redux-saga/effects';

import { validateAddress } from '../../utils/api';
import { ADDRESS_VALIDATE_REQUESTED } from '../actions/addressValidateTypes';
import {
  addressValidateSuccess,
  addressValidateError,
} from '../actions/addressValidate';

export function* addressValidate(data) {
  try {
    const addressValidateCall = yield call(validateAddress, data);
    yield put(addressValidateSuccess(addressValidateCall));
  } catch (error) {
    yield put(addressValidateError(error));
  }
}

export function* watchAddressValidate() {
  yield takeLatest(ADDRESS_VALIDATE_REQUESTED, addressValidate);
}
