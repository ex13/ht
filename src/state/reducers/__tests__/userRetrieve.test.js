import userRetrieve from '../userRetrieve';
import {
  userRetrieveRequest,
  userRetrieveSuccess,
  userRetrieveError,
  userRetrieveUnload,
} from '../../actions/userRetrieve';

describe('userRetrieve reducer', () => {
  let state;

  beforeEach(() => {
    state = {
      user: {},
      isLoading: false,
      error: null,
    };
  });

  it('should handle the request with a correct resulting state.', () => {
    const expectedState = { ...state, isLoading: true };

    expect(userRetrieve(state, userRetrieveRequest({}))).toEqual(expectedState);
  });

  it('should handle the success with a correct resulting state.', () => {
    const expectedState = { ...state, user: { name: 'Test' } };

    expect(userRetrieve(state, userRetrieveSuccess(expectedState.user))).toEqual(expectedState);
  });

  it('should handle the error with a correct resulting state.', () => {
    const error = 'Error';
    const expectedState = { ...state, error };

    expect(userRetrieve(state, userRetrieveError(error))).toEqual(expectedState);
  });

  it('should handle the unload action with a correct resulting state.', () => {
    const expectedState = {};

    expect(userRetrieve(state, userRetrieveUnload())).toEqual(expectedState);
  });
});
