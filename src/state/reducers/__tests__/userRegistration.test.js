import userRegistration from '../userRegistration';
import {
  userRegistrationRequest,
  userRegistrationSuccess,
  userRegistrationError,
  userRegistrationUnload,
} from '../../actions/userRegistration';

describe('userRegistration reducer', () => {
  let state;

  beforeEach(() => {
    state = {
      userCreated: null,
      isLoading: false,
      error: null,
    };
  });

  it('should handle the request with a correct resulting state.', () => {
    const expectedState = { ...state, isLoading: true };

    expect(userRegistration(state, userRegistrationRequest({}))).toEqual(expectedState);
  });

  it('should handle the success with a correct resulting state.', () => {
    const expectedState = { ...state, userCreated: true };

    expect(userRegistration(state, userRegistrationSuccess())).toEqual(expectedState);
  });

  it('should handle the error with a correct resulting state.', () => {
    const error = 'Error';
    const expectedState = { ...state, error };

    expect(userRegistration(state, userRegistrationError(error))).toEqual(expectedState);
  });

  it('should handle the unload with a correct resulting state.', () => {
    const expectedState = {};

    expect(userRegistration(state, userRegistrationUnload())).toEqual(expectedState);
  });
});
