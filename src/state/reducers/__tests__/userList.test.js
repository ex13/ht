import userList from '../userList';
import {
  userListRequest,
  userListReceive,
  userListError,
} from '../../actions/userList';

describe('userList reducer', () => {
  let state;

  beforeEach(() => {
    state = {
      userList: [],
      isLoading: false,
      error: null,
    };
  });

  it('should handle the request with a correct resulting state.', () => {
    const expectedState = { ...state, isLoading: true };

    expect(userList(state, userListRequest({}))).toEqual(expectedState);
  });

  it('should handle the success with a correct resulting state.', () => {
    const expectedState = { ...state, userList: [{ id: '1' }, { id: '2' }] };

    expect(userList(state, userListReceive(expectedState.userList))).toEqual(expectedState);
  });

  it('should handle the error with a correct resulting state.', () => {
    const error = 'Error';
    const expectedState = { ...state, error };

    expect(userList(state, userListError(error))).toEqual(expectedState);
  });
});
