import userDelete from '../userDelete';
import {
  userDeleteRequest,
  userDeleteSuccess,
  userDeleteError,
  userDeleteUnload,
} from '../../actions/userDelete';

describe('userDelete reducer', () => {
  let state;

  beforeEach(() => {
    state = {
      userDeleted: null,
      isLoading: false,
      error: null,
    };
  });

  it('should handle the request with a correct resulting state.', () => {
    const expectedState = { ...state, isLoading: true };

    expect(userDelete(state, userDeleteRequest({}))).toEqual(expectedState);
  });

  it('should handle the success with a correct resulting state.', () => {
    const expectedState = { ...state, userDeleted: true };

    expect(userDelete(state, userDeleteSuccess())).toEqual(expectedState);
  });

  it('should handle the error with a correct resulting state.', () => {
    const error = 'Error';
    const expectedState = { ...state, error };

    expect(userDelete(state, userDeleteError(error))).toEqual(expectedState);
  });

  it('should handle the unload action with a correct resulting state.', () => {
    const expectedState = { ...state };

    expect(userDelete(state, userDeleteUnload())).toEqual(expectedState);
  });
});
