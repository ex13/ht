import addressValidate from '../addressValidate';
import {
  addressValidateRequest,
  addressValidateSuccess,
  addressValidateError,
  addressValidateUnload,
} from '../../actions/addressValidate';

describe('addressValidate reducer', () => {
  let state;

  beforeEach(() => {
    state = {
      validated: true,
      isLoading: false,
      error: null,
    };
  });

  it('should handle the request with a correct resulting state.', () => {
    const expectedState = { ...state, isLoading: true };

    expect(addressValidate(state, addressValidateRequest())).toEqual(expectedState);
  });

  it('should handle the success with a correct resulting state.', () => {
    const expectedState = { ...state, validated: true };

    expect(addressValidate(state, addressValidateSuccess())).toEqual(expectedState);
  });

  it('should handle the error with a correct resulting state.', () => {
    const error = 'Error';
    const expectedState = { ...state, error, validated: false };

    expect(addressValidate(state, addressValidateError(error))).toEqual(expectedState);
  });

  it('should handle the unload action with a correct resulting state.', () => {
    const expectedState = { ...state };

    expect(addressValidate(state, addressValidateUnload())).toEqual(expectedState);
  });
});
