import userUpdate from '../userUpdate';
import {
  userUpdateRequest,
  userUpdateSuccess,
  userUpdateError,
  userUpdateUnload,
} from '../../actions/userUpdate';

describe('userUpdate reducer', () => {
  let state;

  beforeEach(() => {
    state = {
      userUpdated: null,
      isLoading: false,
      error: null,
    };
  });

  it('should handle the request with a correct resulting state.', () => {
    const expectedState = { ...state, isLoading: true };

    expect(userUpdate(state, userUpdateRequest({}))).toEqual(expectedState);
  });

  it('should handle the success with a correct resulting state.', () => {
    const expectedState = { ...state, userUpdated: true };

    expect(userUpdate(state, userUpdateSuccess(expectedState.userUpdated))).toEqual(expectedState);
  });

  it('should handle the error with a correct resulting state.', () => {
    const error = 'Error';
    const expectedState = { ...state, error };

    expect(userUpdate(state, userUpdateError(error))).toEqual(expectedState);
  });

  it('should handle the unload action with a correct resulting state.', () => {
    const expectedState = {};

    expect(userUpdate(state, userUpdateUnload())).toEqual(expectedState);
  });
});
