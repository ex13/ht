import {
  USER_LIST_REQUESTED,
  USER_LIST_RECEIVED,
  USER_LIST_ERROR,
} from '../actions/userListTypes';

const initialState = {
  userList: [],
  isLoading: false,
  error: null,
};

export default function userList(state = initialState, action) {
  switch (action.type) {
    case USER_LIST_REQUESTED:
      return { ...state, isLoading: true, error: null };
    case USER_LIST_RECEIVED:
      return { ...state, isLoading: false, userList: action.userList };
    case USER_LIST_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
}
