import {
  USER_DELETE_REQUESTED,
  USER_DELETE_SUCCESS,
  USER_DELETE_ERROR,
  USER_DELETE_UNLOAD,
} from '../actions/userDeleteTypes';

const initialState = {
  userDeleted: null,
  isLoading: false,
  error: null,
};

export default function userDelete(state = initialState, action) {
  switch (action.type) {
    case USER_DELETE_REQUESTED:
      return { ...state, isLoading: true, error: null };
    case USER_DELETE_SUCCESS:
      return { ...state, isLoading: false, userDeleted: true };
    case USER_DELETE_ERROR:
      return { ...state, isLoading: false, error: action.error };
    case USER_DELETE_UNLOAD:
      return initialState;
    default:
      return state;
  }
}
