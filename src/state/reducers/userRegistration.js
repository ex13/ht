import {
  USER_REGISTRATION_REQUESTED,
  USER_REGISTRATION_SUCCESS,
  USER_REGISTRATION_ERROR,
  USER_REGISTRATION_UNLOAD,
} from '../actions/userRegistrationTypes';

const initialState = {
  userCreated: null,
  isLoading: false,
  error: null,
};

export default function userRegistration(state = initialState, action) {
  switch (action.type) {
    case USER_REGISTRATION_REQUESTED:
      return { ...state, isLoading: true, error: null };
    case USER_REGISTRATION_SUCCESS:
      return { ...state, isLoading: false, userCreated: true };
    case USER_REGISTRATION_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    case USER_REGISTRATION_UNLOAD:
      return {};
    default:
      return state;
  }
}
