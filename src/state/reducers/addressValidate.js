import {
  ADDRESS_VALIDATE_REQUESTED,
  ADDRESS_VALIDATE_SUCCESS,
  ADDRESS_VALIDATE_ERROR,
  ADDRESS_VALIDATE_UNLOAD,
} from '../actions/addressValidateTypes';

const initialState = {
  validated: true,
  isLoading: false,
  error: null,
};

export default function addressValidate(state = initialState, action) {
  switch (action.type) {
    case ADDRESS_VALIDATE_REQUESTED:
      return { ...state, isLoading: true, error: null };
    case ADDRESS_VALIDATE_SUCCESS:
      return { ...state, isLoading: false, validated: true };
    case ADDRESS_VALIDATE_ERROR:
      return { isLoading: false, error: action.error, validated: false };
    case ADDRESS_VALIDATE_UNLOAD:
      return state;
    default:
      return state;
  }
}
