import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import user from './userRetrieve';
import userList from './userList';
import userRegistration from './userRegistration';
import userUpdate from './userUpdate';
import userDelete from './userDelete';
import addressValidate from './addressValidate';

export default combineReducers({
  user,
  userList,
  userRegistration,
  userUpdate,
  userDelete,
  addressValidate,
  router: routerReducer,
});
