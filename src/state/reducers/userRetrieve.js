import {
  USER_REQUESTED,
  USER_RECEIVED,
  USER_ERROR,
  USER_UNLOAD,
} from '../actions/userRetrieveTypes';

const initialState = {
  user: {},
  isLoading: false,
  error: null,
};

export default function user(state = initialState, action) {
  switch (action.type) {
    case USER_REQUESTED:
      return { ...state, isLoading: true, error: null };
    case USER_RECEIVED:
      return { ...state, isLoading: false, user: action.user };
    case USER_ERROR:
      return { ...state, isLoading: false, error: action.error };
    case USER_UNLOAD:
      return {};
    default:
      return state;
  }
}
