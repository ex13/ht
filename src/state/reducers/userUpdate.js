import {
  USER_UPDATE_REQUESTED,
  USER_UPDATE_SUCCESS,
  USER_UPDATE_ERROR,
  USER_UPDATE_UNLOAD,
} from '../actions/userUpdateTypes';

const initialState = {
  userUpdated: null,
  isLoading: false,
  error: null,
};

export default function userUpdate(state = initialState, action) {
  switch (action.type) {
    case USER_UPDATE_REQUESTED:
      return { ...state, isLoading: true, error: null };
    case USER_UPDATE_SUCCESS:
      return { ...state, isLoading: false, userUpdated: true };
    case USER_UPDATE_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    case USER_UPDATE_UNLOAD:
      return {};
    default:
      return state;
  }
}
