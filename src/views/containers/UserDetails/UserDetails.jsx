import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import get from 'lodash.get';
import { Col } from '@smooth-ui/core-sc';

import Loader from '../../components/Loader';
import BackButton from '../../components/BackButton';
import Button from '../../components/shared/StyledButton';
import Row from '../../components/shared/StyledRow';
import { userRetrieveRequest, userRetrieveUnload } from '../../../state/actions/userRetrieve';
import { ROUTE_USER_EDIT } from '../../../constants/routes';
import { cta as copy } from '../../../assets/globalCopy.json';

class UserDetails extends Component {
  componentWillMount() {
    const { match: { params: { id } }, retrieveUser } = this.props;

    if (id) {
      retrieveUser(id);
    }
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    const { isLoading, user } = this.props;

    if (isLoading || !user) {
      return <Loader />;
    }

    return (
      <Fragment>
        <Row>
          <Col>
            <BackButton />
            <Link to={`${ROUTE_USER_EDIT}/${get(user, 'id')}`}>
              <Button>{copy.edit}</Button>
            </Link>
          </Col>
        </Row>
        <Row>
          <Col>
            <ul>
              <li>{get(user, 'metadata.name')}</li>
              <li>{get(user, 'email')}</li>
              <li>{get(user, 'metadata.street')} {get(user, 'metadata.house')}</li>
              <li>{get(user, 'metadata.city')}</li>
              <li>{get(user, 'metadata.zip')}</li>
            </ul>
          </Col>
        </Row>
      </Fragment>
    );
  }
}

UserDetails.propTypes = {
  retrieveUser: PropTypes.func.isRequired,
  onUnload: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.node,
    }).isRequired,
  }).isRequired,
  isLoading: PropTypes.bool,
  user: PropTypes.shape({
    id: PropTypes.string,
    email: PropTypes.string,
    metadata: PropTypes.shape({
      name: PropTypes.string,
      city: PropTypes.string,
      street: PropTypes.string,
      house: PropTypes.string,
      zip: PropTypes.string,
    }),
  }),
};

const mapStateToProps = store => {
  const { user: { user, isLoading } } = store;

  return { user, isLoading };
};

const mapDispatchToProps = dispatch => ({
  retrieveUser: id => dispatch(userRetrieveRequest(id)),
  onUnload: () => dispatch(userRetrieveUnload()),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserDetails);
