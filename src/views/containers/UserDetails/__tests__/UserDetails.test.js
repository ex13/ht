import React from 'react';
import { shallow } from 'enzyme';

import UserDetails from '../UserDetails';

describe('UserDetails component', () => {
  const props = {
    store: {
      getState: () => ({
        user: {
          user: {},
          isLoading: false,
          error: null,
        },
      }),
      subscribe: jest.fn(),
      dispatch: jest.fn(),
    },
    isLoading: false,
    retrieveUser: jest.fn(),
    onUnload: jest.fn(),
    match: {
      params: { id: '12345' },
    },
  };

  it('should render the UserDetails component.', () => {
    const renderedComponent = shallow(<UserDetails {...props} />).dive();
    expect(renderedComponent).toMatchSnapshot();
  });
});
