import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Col } from '@smooth-ui/core-sc';

import { userListRequest } from '../../../state/actions/userList';
import { userDeleteRequest, userDeleteUnload } from '../../../state/actions/userDelete';
import {
  ROUTE_USER_REGISTRATION,
  ROUTE_USER_EDIT,
  ROUTE_USER_DETAILS,
} from '../../../constants/routes';
import Loader from '../../components/Loader';
import DeleteButton from '../../components/DeleteButton';
import Button from '../../components/shared/StyledButton';
import Row from '../../components/shared/StyledRow';
import { userList as copy, cta } from '../../../assets/globalCopy.json';

class UserList extends Component {
  constructor(props) {
    super(props);

    this.deleteUser = this.deleteUser.bind(this);
  }

  componentDidMount() {
    this.props.retrieveUserList();
  }

  componentDidUpdate() {
    // If a user was deleted, update the user list
    if (this.props.userDeleted) {
      this.props.onUnload();
      this.props.retrieveUserList();
    }
  }

  deleteUser(id) {
    this.props.deleteUser(id);
  }

  render() {
    const { userList, isLoading } = this.props;

    if (isLoading) {
      return <Loader />;
    }

    if (!userList || !userList.length) {
      return (
        <Fragment>
          {copy.noUsers}
        </Fragment>
      );
    }

    return (
      <Fragment>
        <Row>
          <Col>
            <Link to={ROUTE_USER_REGISTRATION}>
              <Button width={1}>{copy.addUser}</Button>
            </Link>
          </Col>
        </Row>

        {userList.map(user => {
          const {
            id, email, metadata: {
              name, city, street, house, zip,
            },
          } = user;
          return (
            <Row key={id} alignItems="center" justifyContent="space-between">
              <Col
                xs={12}
                md="auto"
                textAlign={{ xs: 'center', md: 'left' }}
              >
                <div>{name}</div>
                <div>{email}</div>
                <div>{street} {house}, {city}, {zip}</div>
              </Col>
              <Col
                xs={12}
                md="auto"
                textAlign={{ xs: 'center', md: 'right' }}
                mt={{ xs: '1rem', md: '0' }}
              >
                <Link to={`${ROUTE_USER_EDIT}/${id}`}>
                  <Button>{cta.edit}</Button>
                </Link>
                <Link to={`${ROUTE_USER_DETAILS}/${id}`}>
                  <Button ml={1}>{cta.details}</Button>
                </Link>
                <DeleteButton onDeleteClick={() => this.deleteUser(id)} />
              </Col>
            </Row>
          );
        })}
      </Fragment>
    );
  }
}

UserList.propTypes = {
  retrieveUserList: PropTypes.func.isRequired,
  deleteUser: PropTypes.func.isRequired,
  onUnload: PropTypes.func.isRequired,
  userList: PropTypes.array,
  isLoading: PropTypes.bool,
  userDeleted: PropTypes.bool,
};

const mapStateToProps = store => {
  const { userList, userDelete } = store;

  return {
    userList: userList.userList,
    isLoading: userList.isLoading || userDelete.isLoading,
    userDeleted: userDelete.userDeleted,
  };
};

const mapDispatchToProps = dispatch => ({
  retrieveUserList: () => dispatch(userListRequest()),
  deleteUser: id => dispatch(userDeleteRequest(id)),
  onUnload: () => dispatch(userDeleteUnload()),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserList);
