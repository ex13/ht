import React from 'react';
import { shallow } from 'enzyme';

import UserList from '../UserList';

describe('UserList component', () => {
  const props = {
    store: {
      getState: () => ({
        userList: {
          userList: [],
          isLoading: false,
        },
        userDelete: {
          userDeleted: false,
          isLoading: false,
        },
      }),
      subscribe: jest.fn(),
      dispatch: jest.fn(),
    },
    retrieveUserList: jest.fn(),
    deleteUser: jest.fn(),
    onUnload: jest.fn(),
    userList: [],
    isLoading: false,
    userDeleted: false,
  };

  it('should render the UserList component.', () => {
    const renderedComponent = shallow(<UserList {...props} />).dive();
    expect(renderedComponent).toMatchSnapshot();
  });
});
