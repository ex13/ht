import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { withFormik } from 'formik';
import {
  FormGroup, Label, Input, Alert, Row, Col, Box,
} from '@smooth-ui/core-sc';

import { userRegistrationRequest, userRegistrationUnload } from '../../../state/actions/userRegistration';
import { userRetrieveRequest, userRetrieveUnload } from '../../../state/actions/userRetrieve';
import { userUpdateRequest, userUpdateUnload } from '../../../state/actions/userUpdate';
import { addressValidateRequest, addressValidateUnload } from '../../../state/actions/addressValidate';
import Loader from '../../components/Loader';
import BackButton from '../../components/BackButton';
import Button from '../../components/shared/StyledButton';
import userFormPropTypes from './userFormPropTypes';
import { REGISTRATION_FORM_CONFIG } from '../../../constants/formConfig';
import { ROUTE_HOME } from '../../../constants/routes';
import { userForm as copy } from '../../../assets/globalCopy.json';

class UserForm extends Component {
  constructor(props) {
    super(props);

    this.isUserUpdated = this.isUserUpdated.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }

  componentWillMount() {
    const userId = this.props.match.params.id;

    // If URL contains a userId, this form switches into an "edit" mode,
    // And retrieves the requested user's data into the fields.
    // Otherwhise this view acts as a new user registration form.
    if (userId) {
      this.props.retrieveUser(userId);
    }
  }

  componentWillReceiveProps(nextProps) {
    this.isUserUpdated(nextProps.userCreated, nextProps.userUpdated);
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  isUserUpdated(userCreated, userUpdated) {
    // Redirect to user list after a user has been successfully created or updated.
    if (userCreated || userUpdated) {
      setTimeout(() => {
        this.props.push(ROUTE_HOME);
      }, 2000);
    }
  }

  handleBlur(e) {
    const {
      handleBlur,
      validateAddress,
      values: {
        city, street, house, zip,
      },
    } = this.props;

    // Validate address through Google Maps API onBlur if all address fields are filled out.
    if (city && street && house && zip) {
      validateAddress({
        city, street, house, zip,
      });
    }

    handleBlur(e);
  }

  render() {
    const {
      values: {
        name, email, city, street, house, zip,
      },
      isLoading,
      errors,
      isSubmitting,
      handleChange,
      handleBlur,
      handleSubmit,
      isValid,
      userCreated,
      userUpdated,
      validated,
      match: { params: { id } },
    } = this.props;
    const isSubmitDisabled = isSubmitting || isLoading || !isValid || !validated;

    // If it's an "edit" mode of the form, show loader until the initial form values load.
    if (id && isLoading) {
      return <Loader />;
    }

    if (userCreated) {
      return <Alert variant="success">{copy.userCreated}</Alert>;
    }

    if (userUpdated) {
      return <Alert variant="success">{copy.userUpdated}</Alert>;
    }

    return (
      <Row>
        <Col>
          <BackButton />

          <form onSubmit={handleSubmit}>
            <Box mt={4} mb={4}>{copy.note}</Box>
            <FormGroup>
              <Label htmlFor="form-group-input-name">{copy.name}</Label>
              <Input
                control
                name="name"
                id="form-group-input-name"
                value={name}
                valid={!errors.name}
                onBlur={handleBlur}
                onChange={handleChange}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor="form-group-input-email">{copy.email}</Label>
              <Input
                control
                name="email"
                type="email"
                id="form-group-input-email"
                value={email}
                valid={!errors.email}
                onBlur={handleBlur}
                onChange={handleChange}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor="form-group-input-city">{copy.city}</Label>
              <Input
                control
                name="city"
                id="form-group-input-city"
                value={city}
                valid={!errors.city && validated}
                onBlur={this.handleBlur}
                onChange={handleChange}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor="form-group-input-street">{copy.street}</Label>
              <Input
                control
                name="street"
                id="form-group-input-street"
                value={street}
                valid={!errors.street && validated}
                onBlur={this.handleBlur}
                onChange={handleChange}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor="form-group-input-house">{copy.house}</Label>
              <Input
                control
                name="house"
                type="number"
                id="form-group-input-house"
                value={house}
                valid={!errors.house && validated}
                onBlur={this.handleBlur}
                onChange={handleChange}
              />
            </FormGroup>
            <FormGroup>
              <Label htmlFor="form-group-input-zip">{copy.zip}</Label>
              <Input
                control
                name="zip"
                id="form-group-input-zip"
                value={zip}
                valid={!errors.zip && validated}
                onBlur={this.handleBlur}
                onChange={handleChange}
              />
            </FormGroup>

            {!validated &&
              <Alert variant="danger" mt={4}>{copy.addressNotValid}</Alert>
            }

            <Button type="submit" width={1} height="3rem" mt={4} disabled={isSubmitDisabled}>
              {copy.submit}
            </Button>
          </form>
        </Col>
      </Row>
    );
  }
}

UserForm.propTypes = userFormPropTypes;

const mapStateToProps = store => {
  const {
    userRegistration, userUpdate, user, addressValidate: { validated },
  } = store;

  return {
    userCreated: userRegistration.userCreated,
    userUpdated: userUpdate.userUpdated,
    user: user.user,
    isLoading: userRegistration.isLoading || userUpdate.isLoading || user.isLoading,
    error: userRegistration.error || userUpdate.error,
    validated,
  };
};

const mapDispatchToProps = dispatch => ({
  registerUser: data => dispatch(userRegistrationRequest(data)),
  retrieveUser: id => dispatch(userRetrieveRequest(id)),
  updateUser: (id, data) => dispatch(userUpdateRequest(id, data)),
  validateAddress: data => dispatch(addressValidateRequest(data)),
  onUnload: () => {
    dispatch(userRegistrationUnload());
    dispatch(userRetrieveUnload());
    dispatch(userUpdateUnload());
    dispatch(addressValidateUnload());
  },
  push: path => dispatch(push(path)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withFormik(REGISTRATION_FORM_CONFIG)(UserForm));
