import PropTypes from 'prop-types';

export default {
  isSubmitting: PropTypes.bool.isRequired,
  push: PropTypes.func.isRequired,
  retrieveUser: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  validateAddress: PropTypes.func.isRequired,
  onUnload: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.node,
    }).isRequired,
  }).isRequired,
  isLoading: PropTypes.bool,
  user: PropTypes.shape({
    id: PropTypes.string,
    email: PropTypes.string,
    metadata: PropTypes.shape({
      name: PropTypes.string,
      city: PropTypes.string,
      street: PropTypes.string,
      house: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
      ]),
      zip: PropTypes.string,
    }),
  }),
  values: PropTypes.shape({
    name: PropTypes.string,
    email: PropTypes.string,
    city: PropTypes.string,
    street: PropTypes.string,
    house: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]),
    zip: PropTypes.string,
  }),
  errors: PropTypes.object,
  isValid: PropTypes.bool,
  validated: PropTypes.bool,
  userCreated: PropTypes.bool,
  userUpdated: PropTypes.bool,
};
