import React from 'react';
import { shallow } from 'enzyme';

import UserForm from '../UserForm';

const withFormik = jest.fn(); // eslint-disable-line no-unused-vars

describe('UserForm component', () => {
  const props = {
    store: {
      getState: () => ({
        userRegistration: {},
        userUpdate: {},
        user: {},
        addressValidate: { validated: true },
      }),
      subscribe: jest.fn(),
      dispatch: jest.fn(),
    },
    values: {},
    touched: {},
    errors: {},
    isSubmitting: false,
    handleChange: jest.fn(),
    handleBlur: jest.fn(),
    handleSubmit: jest.fn(),
    push: jest.fn(),
    errorDismiss: jest.fn(),
    isLoading: false,
    error: null,
    retrieveUser: jest.fn(),
    validateAddress: jest.fn(),
    onUnload: jest.fn(),
    match: { params: {} },
    isValid: true,
    validated: true,
    userCreated: null,
    userUpdated: null,
  };

  it('should render the UserForm component.', () => {
    const renderedComponent = shallow(<UserForm {...props} />).dive();
    expect(renderedComponent).toMatchSnapshot();
  });
});
