import React from 'react';
import styled from 'styled-components';

import LogoImage from '../../../../assets/logo.png';

const StyledContainer = styled.div`
  margin: 1rem 0 0;
  text-align: center;
  transition: all .2s ease-in-out;

  img {
    max-height: 200px;
  }

  :hover {
    transform: scale(1.1);
  }
`;

const StyledLogo = props => (
  <StyledContainer>
    <img src={LogoImage} alt="logo" {...props} />
  </StyledContainer>
);

export default StyledLogo;
