import React from 'react';
import { Link } from 'react-router-dom';

import Logo from './styled/StyledLogo';
import { ROUTE_HOME } from '../../../constants/routes';

export default () => (
  <Link to={ROUTE_HOME}>
    <Logo />
  </Link>
);
