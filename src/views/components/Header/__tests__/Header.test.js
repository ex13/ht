import React from 'react';
import { Link, BrowserRouter as Router } from 'react-router-dom';
import { shallow } from 'enzyme';

import Header from '../Header';

describe('Header component', () => {
  it('should render the Header component.', () => {
    const tree = shallow(<Header />);
    const renderedComponent = shallow(
      <Router>
        <Header />
      </Router>
    ).dive();

    expect(tree.find(Link)).toHaveLength(1);
    expect(renderedComponent).toMatchSnapshot();
  });
});
