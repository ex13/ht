import React from 'react';
import renderer from 'react-test-renderer';

import StyledLoader from '../StyledLoader';

describe('StyledLoader component', () => {
  it('should render the StyledLoader element.', () => {
    const tree = renderer.create(
      <StyledLoader />
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
