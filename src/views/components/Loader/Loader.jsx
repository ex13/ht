import React from 'react';
import { Row } from '@smooth-ui/core-sc';

import StyledLoader from './styled/StyledLoader';

export default () => (
  <Row justifyContent="center">
    <StyledLoader>
      <div></div>
      <div></div>
    </StyledLoader>
  </Row>
);
