import React from 'react';
import { shallow } from 'enzyme';

import Loader from '../Loader';

describe('Loader component', () => {
  it('should render the Loader component.', () => {
    const renderedComponent = shallow(<Loader />).dive();
    expect(renderedComponent).toMatchSnapshot();
  });
});
