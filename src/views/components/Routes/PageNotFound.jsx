import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';

import { ROUTE_HOME } from '../../../constants/routes';

const PageNotFound = props => (
  <Redirect
    to={{
      pathname: ROUTE_HOME,
      state: { from: props.location },
    }}
  />
);

PageNotFound.propTypes = {
  location: PropTypes.object.isRequired,
};

export default PageNotFound;
