import React from 'react';
import { Switch, Route } from 'react-router-dom';

import App from '../App';
import UserList from '../../containers/UserList';
import UserForm from '../../containers/UserForm';
import UserDetails from '../../containers/UserDetails';
import PageNotFound from './PageNotFound';
import {
  ROUTE_HOME,
  ROUTE_USER_REGISTRATION,
  ROUTE_USER_EDIT,
  ROUTE_USER_DETAILS,
} from '../../../constants/routes';

export default () => (
  <App>
    <Switch>
      <Route exact path={ROUTE_HOME} component={UserList} />
      <Route exact path={ROUTE_USER_REGISTRATION} component={UserForm} />
      <Route exact path={`${ROUTE_USER_EDIT}/:id`} component={UserForm} />
      <Route exact path={`${ROUTE_USER_DETAILS}/:id`} component={UserDetails} />
      <Route component={PageNotFound} />
    </Switch>
  </App>
);
