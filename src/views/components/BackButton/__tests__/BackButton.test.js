import React from 'react';
import renderer from 'react-test-renderer';
import { BrowserRouter as Router } from 'react-router-dom';

import BackButton from '../BackButton';

describe('BackButton component', () => {
  it('should render the <BackButton> component.', () => {
    const tree = renderer.create(
      <Router><BackButton /></Router>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
