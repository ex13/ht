import React from 'react';
import { Link } from 'react-router-dom';

import StyledButton from '../shared/StyledButton';
import { ROUTE_HOME } from '../../../constants/routes';
import { cta as copy } from '../../../assets/globalCopy.json';

export default () => (
  <Link to={ROUTE_HOME}>
    <StyledButton mr={1}>{copy.back}</StyledButton>
  </Link>
);
