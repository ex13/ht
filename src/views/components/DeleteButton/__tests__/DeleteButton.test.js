import React from 'react';
import EnzymeToJson from 'enzyme-to-json';
import { mount } from 'enzyme';

import DeleteButton from '../DeleteButton';

describe('DeleteButton component', () => {
  const props = {
    onDeleteClick: jest.fn(),
  };

  it('should render the <DeleteButton> component.', () => {
    const tree = mount(
      <DeleteButton {...props} />
    );
    expect(EnzymeToJson(tree)).toMatchSnapshot();
  });
});
