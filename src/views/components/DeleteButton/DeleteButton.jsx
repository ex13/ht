import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  Toggler,
  Modal,
  ModalDialog,
  ModalContent,
  Typography,
  ModalBody,
  ModalFooter,
} from '@smooth-ui/core-sc';

import StyledButton from '../shared/StyledButton';
import { cta as copy } from '../../../assets/globalCopy.json';

export const DeleteButton = ({ onDeleteClick }) => (
  <Toggler>
    {({ toggled, onToggle }) => (
      <Fragment>
        <StyledButton ml={1} variant="danger" onClick={() => onToggle(true)}>
          {copy.delete}
        </StyledButton>
        <Modal opened={toggled} onClose={() => onToggle(false)}>
          <ModalDialog>
            <ModalContent>
              <ModalBody>
                <Typography variant="h5" m={0}>
                  {copy.areYouSure}
                </Typography>
              </ModalBody>
              <ModalFooter>
                <StyledButton
                  variant="danger"
                  onClick={() => {
                    onDeleteClick();
                    onToggle(false);
                  }}
                >
                  {copy.deleteYes}
                </StyledButton>
                <StyledButton variant="secondary" onClick={() => onToggle(false)}>
                  {copy.deleteNo}
                </StyledButton>
              </ModalFooter>
            </ModalContent>
          </ModalDialog>
        </Modal>
      </Fragment>
    )}
  </Toggler>
);

DeleteButton.propTypes = {
  onDeleteClick: PropTypes.func.isRequired,
};

export default DeleteButton;
