import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { ThemeProvider } from 'styled-components';
import { Grid, Row, Col } from '@smooth-ui/core-sc';

import Header from '../Header';
import defaultTheme from '../../../styles/defaultTheme';
import '../../../styles/globalStyles';
import { app as copy } from '../../../assets/globalCopy.json';

export const App = props => (
  <ThemeProvider theme={defaultTheme}>
    <div>
      <Helmet
        defaultTitle={copy.defaultTitle}
        titleTemplate={copy.defaultTitleTpl}
        meta={[{ name: 'description', content: copy.description }]}
      />
      <Grid>
        <Row>
          <Col>
            <Header />
          </Col>
        </Row>
        <Row>
          <Col>
            {props.children}
          </Col>
        </Row>
      </Grid>
    </div>
  </ThemeProvider>
);

App.propTypes = {
  children: PropTypes.node.isRequired,
};

export default App;
