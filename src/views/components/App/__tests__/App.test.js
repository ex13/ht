import React from 'react';
import { shallow } from 'enzyme';
import { BrowserRouter as Router } from 'react-router-dom';

import App from '../App';

describe('App component', () => {
  const props = {
    children: <div>Child component</div>,
  };

  it('should render the app container with child components.', () => {
    const renderedComponent = shallow(<Router><App {...props} /></Router>).dive();
    expect(renderedComponent).toMatchSnapshot();
  });
});
