import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Button } from '@smooth-ui/core-sc';

const StyledContainer = styled(Button)`
  font-weight: 300;
  text-transform: uppercase;
`;

const StyledButton = props => (
  <StyledContainer variant="dark" {...props}>
    {props.children}
  </StyledContainer>
);

StyledButton.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default StyledButton;
