import React from 'react';
import { shallow } from 'enzyme';

import StyledRow from '../StyledRow';

describe('StyledRow component', () => {
  it('should render the StyledRow component.', () => {
    const renderedComponent = shallow(<StyledRow />).dive();
    expect(renderedComponent).toMatchSnapshot();
  });
});
