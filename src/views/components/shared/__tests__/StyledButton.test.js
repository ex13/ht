import React from 'react';
import { shallow } from 'enzyme';

import StyledButton from '../StyledButton';

describe('StyledButton component', () => {
  it('should render the StyledButton component.', () => {
    const renderedComponent = shallow(<StyledButton />).dive();
    expect(renderedComponent).toMatchSnapshot();
  });
});
