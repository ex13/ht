import React from 'react';
import PropTypes from 'prop-types';
import { styled, Row } from '@smooth-ui/core-sc';

const StyledContainer = styled(Row)`
  align-items: center;
  border-bottom: 1px solid ${props => props.theme.listBorderColor};
  color: ${props => props.theme.listTextColor};
  transition: background-color .2s;
  padding: 1rem 0;
  &:hover {
    background-color: ${props => props.theme.bgColorSecondary};
  }
`;

const StyledRow = props => (
  <StyledContainer {...props}>
    {props.children}
  </StyledContainer>
);

StyledRow.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default StyledRow;
